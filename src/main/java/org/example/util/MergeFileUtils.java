package org.example.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import static org.example.util.FileUtils.writeNumberLn;
import static org.example.util.FileUtils.writeStringLn;

/**
 * Утилыты сортировки включением файлов.
 *
 * @author Shuvi
 */
public class MergeFileUtils {

    /**
     * Объединяет данные типа Integer в один, сортируя их включением.
     *
     * @param inputFiles Файлы, в которых находятся данные.
     * @param out        Файл, в который надо записать данные.
     */
    public static void mergeFilesWithTypeInteger(
            List<CustomBufferedReader> inputFiles,
            BufferedWriter out
    ) throws IOException {
        while (nextValues(inputFiles)) {
            // Находим минимальное значение.
            CustomBufferedReader min = inputFiles.stream()
                    .filter(input -> input.getCurrentValue() != null)
                    .min(Comparator.comparing((r) -> Integer.parseInt(r.getCurrentValue())))
                    .orElseThrow();

            // Говорим каждому CustomBufferedReader удержать значение,
            // кроме хранящего минимальное значение.
            keepCustomBufferReaders(inputFiles, min);

            writeNumberLn(out, Integer.parseInt(min.getCurrentValue()));
        }
    }

    /**
     * Объединяет данные типа String в один, сортируя их включением.
     *
     * @param inputFiles Файлы, в которых находятся данные.
     * @param outFile    Файл, в который надо записать данные.
     */
    public static void mergeFilesWithTypeString(
            List<CustomBufferedReader> inputFiles,
            BufferedWriter outFile
    ) throws IOException {
        while (nextValues(inputFiles)) {
            CustomBufferedReader min = inputFiles.stream()
                    .filter(i -> i.getCurrentValue() != null)
                    .sorted(Comparator.comparing(CustomBufferedReader::getCurrentValue))
                    .findFirst()
                    .orElseThrow();

            keepCustomBufferReaders(inputFiles, min);

            writeStringLn(outFile, min.getCurrentValue());
        }
    }

    /**
     * Говорим бафферам удерживать значение, кроме определённого
     *
     * @param inputFiles Наши входные данные.
     * @param value Значение, которое необходимо пропустить.
     */
    private static void keepCustomBufferReaders(
            List<CustomBufferedReader> inputFiles,
            CustomBufferedReader value
    ) {
        inputFiles.stream()
                .filter(i -> i != value)
                .forEach(CustomBufferedReader::keep);
    }

    /**
     * Данный метод обновляет currentValue у каждого IntegerBufferedReader,
     * только если keep установлен на false, в обратном случае он ничего не меняет.
     *
     * @param inputFiles Файлы, в которых находятся данные.
     * @return Возвращает {false} - если данные во всех файлах были использованы и больше их нет, {true} - наооборот.
     */
    private static boolean nextValues(List<CustomBufferedReader> inputFiles) throws IOException {
        for (CustomBufferedReader inputFile : inputFiles) {
            // Если значение не удерживается, то обновляем его.
            if (!inputFile.ifKeepPreviousValue()) {
                String string = inputFile.readLine();
                inputFile.setCurrentValue(string);
            } else {
                inputFile.unKeep();
            }
        }
        // Проверяем есть ли значения в наших файлах.
        return !inputFiles.stream().allMatch(i -> i.getCurrentValue() == null);
    }
}
