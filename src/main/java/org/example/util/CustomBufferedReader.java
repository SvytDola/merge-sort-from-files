package org.example.util;


import java.io.BufferedReader;
import java.io.Reader;

/**
 * Автоматически конвертирует полученные значения в число.
 * Содержит boolean аттрибут keep, указывающий, что данные не использовались.
 * Содержит currentValue, которое хранит в себе предыдущее число.
 *
 * @author Shuvi
 */
public class CustomBufferedReader extends BufferedReader {

    private boolean keep;
    private String currentValue;

    public CustomBufferedReader(Reader in) {
        super(in);
    }

    /**
     * @return Возвращает предыдущее значение из nextValue.
     */
    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    /**
     * Удерживает currentValue.
     */
    public void keep() {
        keep = true;
    }

    /**
     * Отпускает currentValue
     */
    public void unKeep() {
        keep = false;
    }

    /**
     * Возвращает {true} - если предыдущее значение хотят удержать, {false} - если наооборот.
     */
    public boolean ifKeepPreviousValue() {
        return this.keep;
    }


}
