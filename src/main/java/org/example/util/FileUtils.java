package org.example.util;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Утилиты взаимодействующие c файлами.
 *
 * @author Shuvi
 */
public class FileUtils {

    /**
     * Записывает число в BufferedWriter и добавляет перенос строки.
     *
     * @param bufferedWriter Файл записи.
     * @param number         Число, которое мы хотим записать.
     */
    public static void writeNumberLn(BufferedWriter bufferedWriter, int number) throws IOException {
        String numberString = String.valueOf(number);
        bufferedWriter.write(numberString);
        bufferedWriter.newLine();
    }

    /**
     * Записывает число в BufferedWriter и добавляет перенос строки.
     *
     * @param bufferedWriter Файл записи.
     * @param value          Строка, которую мы хотим записать.
     */
    public static void writeStringLn(BufferedWriter bufferedWriter, String value) throws IOException {
        bufferedWriter.write(value);
        bufferedWriter.newLine();
    }
}
