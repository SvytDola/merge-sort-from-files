package org.example;

import org.apache.commons.cli.*;
import org.apache.commons.cli.CommandLine;
import org.example.util.CustomBufferedReader;
import org.example.util.MergeFileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException, ParseException {
        CommandLine commandLine = buildCommandLine(args);
        List<String> argList = commandLine.getArgList();

        // Файл для записи.
        BufferedWriter outFile = new BufferedWriter(new FileWriter(argList.get(0)));

        // Файлы с данными.
        List<CustomBufferedReader> inputFiles = getInputFiles(argList.stream().skip(1).toList());

        // Проверяем тип данных.
        if (commandLine.hasOption("i")) {
            MergeFileUtils.mergeFilesWithTypeInteger(inputFiles, outFile);
        } else if (commandLine.hasOption("s")) {
            MergeFileUtils.mergeFilesWithTypeString(inputFiles, outFile);
        }

        for (CustomBufferedReader inputFile : inputFiles) {
            inputFile.close();
        }
        outFile.close();
    }

    /**
     * Загружает файлы в стримы.
     *
     * @param paths Список путей до файлов.
     * @return Список данных из файлов.
     */
    private static List<CustomBufferedReader> getInputFiles(List<String> paths) throws FileNotFoundException {
        List<CustomBufferedReader> inputFiles = new ArrayList<>();

        for (String inputFilePath : paths) {
            FileReader fileReader = new FileReader(inputFilePath);
            CustomBufferedReader customBufferedReader = new CustomBufferedReader(fileReader);
            inputFiles.add(customBufferedReader);
        }

        return inputFiles;
    }

    /**
     * Собирает парсер командной строки.
     * @param args Аргументы командной строки.
     * @return Распарсенные данные с комнадной строки.
     */
    private static CommandLine buildCommandLine(String... args) throws ParseException {
        CommandLineParser parser = new DefaultParser();

        Options options = new Options();

        Option integer = Option.builder("i")
                .required(false)
                .build();

        options.addOption(integer);
        Option string = Option.builder("s")
                .required(false)
                .build();
        options.addOption(string);

        return parser.parse(options, args);
    }
}