package org.example;

import org.example.util.CustomBufferedReader;
import org.example.util.MergeFileUtils;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


class MergeFileUtilsTest {

    @Test
    void mergeFiles() throws IOException {
        String[] fileReaders = {"in1.txt", "in2.txt", "in3.txt"};
        List<CustomBufferedReader> list = new ArrayList<>();

        for (String fileReaderName : fileReaders) {
            FileReader fileReader = new FileReader(fileReaderName);
            CustomBufferedReader customBufferedReader = new CustomBufferedReader(fileReader);
            list.add(customBufferedReader);
        }

        BufferedWriter out = new BufferedWriter(new FileWriter("out.txt"));
        MergeFileUtils.mergeFilesWithTypeString(list, out);
    }
}