# Сортировка включением для файлов.

## Setup
Java version: 17
```
mvn clean compile assembly:single
```

## Start
```shell
java -jar .\target\merge-1.0-jar-with-dependencies.jar -s out.txt in1.txt in2.txt in3.txt
```

## Зависимости 
- junit5 # для тестов
- commons-cli # для парсинга аргументов

